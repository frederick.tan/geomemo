package apps.crazy.potato.honey.memozone;

import android.graphics.Bitmap;

/**
 * Created by frede_000 on 30/05/2016.
 */
public class Record {
    public Long id;
    public String title;
    public String dateCreated;
    public String timeCreated;
    public Bitmap attachedImage;
    public String expirationDate;
    public Double latitude;
    public Double longitude;
    public String message;
    public Double radius;

    public Record(Long id_, String title_, String dateCreated_, String timeCreated_, Bitmap attachedImage_, String expirationDate_, Double geoLocationX_, Double getGeoLocationY_, String message_, Double radius_) {
        id = id_;
        title = title_;
        dateCreated = dateCreated_;
        timeCreated = timeCreated_;
        attachedImage = attachedImage_;
        expirationDate = expirationDate_;
        latitude = geoLocationX_;
        longitude = getGeoLocationY_;
        message = message_;
        radius = radius_;
    }
}
