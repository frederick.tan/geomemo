package apps.crazy.potato.honey.memozone;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

/**
 * Definition: Record Editor Fragment
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class ReminderEditorFragment
        extends Fragment
        implements View.OnClickListener
{
    static final int LOCATION_REQUEST = 1;
    static final int EXPIRATION_DATE_REQUEST = 2;
    static final int PHOTO_REQUEST = 3;
    /**
     * Hold a reference to the current animator, so that it can be canceled mid-way.
     */
    private Animator mCurrentAnimator;
    /**
     * The system "short" animation time duration, in milliseconds. This duration is ideal for
     * subtle animations or animations that occur very frequently.
     */
    private int mShortAnimationDuration;
    /** Declaration of all local variables */
    private Long currentId;
    private String currentData;
    private String currentTitle;
    private EditText titleText;
    private EditText bodyText;

    /**Paramters to hold the various Activity return values*/
    /**needed to create the DB object                      */
    /**(1) Map related parameters, initialized to 0s       */
    private Double latitude = -9999.0;
    private Double longitude = -9999.0;
    private Double radius = -9999.0;
    private Bitmap imageAttachment;
    private String expiration;


    public interface EditorOperation extends Serializable {
        //void onNewFileSaveClicked(String title, String data);
        long onNewFileSaveClicked(String title, Bitmap image, String expirationDate, Double geoLocationX, Double getGeoLocationY, String message, Double radius);
        void onExistingFileSaveClicked(Long id, String title, Bitmap image, String expirationDate, Double geoLocationX, Double getGeoLocationY, String message, Double radius);
        void onDeleteClicked();
        void onHomeClicked();
    }
    private EditorOperation editorOperationCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentId = getArguments() != null ? getArguments().getLong("id", -1) : -1;
        currentTitle = getArguments() != null ? getArguments().getString("title") : "";
        currentData = getArguments() != null ? getArguments().getString("data") : "";
        byte[] imageBytes = getArguments() != null ? getArguments().getByteArray("image") : null;
        if (imageBytes != null)
            imageAttachment = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        latitude = getArguments() != null ? getArguments().getDouble("latitude") : -9999.0;
        longitude = getArguments() != null ? getArguments().getDouble("longitude") : -9999.0;
        radius = getArguments() != null ? getArguments().getDouble("radius") : -9999.0;
        Log.i("MemoZone", Long.toString(currentId) + ":" + currentTitle + ":" + currentData +":");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.editor_fragment, container, false);
        Log.d("MemoZone", "O, natawag naman");
        /** Get the title text view */
        titleText = (EditText)view.findViewById(R.id.editorTitle);
        if (titleText != null) {
            titleText.setText(currentTitle);
            Log.d("MemoZone", "Set title: " + currentTitle);
        } else {
            Log.d("MemoZone", "titleText is null???");
        }

        /** Get the reference to the body */
        bodyText = (EditText)(view.findViewById(R.id.noteBody));
        if (bodyText != null) {
            bodyText.setText(currentData);
            bodyText.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {}
                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {}
                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    //saveButton.setEnabled(true);
                }
            });
        }

        /** Set the listeners to the icons */
        ImageView locationImgView;
        ImageView timerImgView;
        ImageView socialNetworkImgView;
        ImageView deleteImageView;
        ImageView saveImageView;
        ImageView cameraImageView;
        ImageView homeImageView;
        locationImgView = (ImageView)view.findViewById(R.id.editorMapsImage);
        if (locationImgView != null) {
            locationImgView.setClickable(true);
            locationImgView.setEnabled(true);
            locationImgView.setOnClickListener(this);
        }
        timerImgView = (ImageView)view.findViewById(R.id.editorTimeImage);
        if (timerImgView != null) {
            timerImgView.setClickable(true);
            timerImgView.setEnabled(true);
            timerImgView.setOnClickListener(this);
        }
        socialNetworkImgView = (ImageView)view.findViewById(R.id.editorNetworkImage);
        if (socialNetworkImgView != null) {
            socialNetworkImgView.setClickable(true);
            socialNetworkImgView.setEnabled(true);
            socialNetworkImgView.setOnClickListener(this);
        }
        deleteImageView = (ImageView)view.findViewById(R.id.editorDeleteImage);
        if (deleteImageView != null) {
            deleteImageView.setClickable(true);
            deleteImageView.setEnabled(true);
            deleteImageView.setOnClickListener(this);
        }
        saveImageView = (ImageView)view.findViewById(R.id.editorSaveImage);
        if (saveImageView != null) {
            saveImageView.setClickable(true);
            saveImageView.setEnabled(true);
            saveImageView.setOnClickListener(this);
        }
        cameraImageView = (ImageView)view.findViewById(R.id.editorCameraImage);
        if (cameraImageView != null) {
            cameraImageView.setClickable(true);
            cameraImageView.setEnabled(true);
            cameraImageView.setOnClickListener(this);
        }
        homeImageView = (ImageView)view.findViewById(R.id.editorHomeImageView);
        if (homeImageView != null) {
            homeImageView.setOnClickListener(this);

        }
        mShortAnimationDuration = 1000;
        view.setTag(currentTitle);
        return view;
    }

    //@override
    public void onAttach(Context context){
        super.onAttach(context);
        Activity activity = null;
        if (context instanceof Activity) {
            activity = (Activity) context;
        } try {
            editorOperationCallback = (EditorOperation)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("The activity " + activity.toString() + " must implement EditorOperation interface.");
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        switch (requestCode) {
            case LOCATION_REQUEST: {
                latitude = data.getDoubleExtra("latitude", -9999.0);
                longitude = data.getDoubleExtra("longitude", -9999.0);
                radius = data.getDoubleExtra("radius", -9999.0);
                break;
            }
            case EXPIRATION_DATE_REQUEST: {
                expiration = data.getStringExtra("expiration");
                break;
            }
            case PHOTO_REQUEST: {
                imageAttachment = BitmapFactory.decodeByteArray(data.getByteArrayExtra("image"), 0, data.getByteArrayExtra("image").length);
                break;
            }
        }
    }
    public void animatedDelete() {
        Log.i("MemoZone", "AnimatedDelete");

        Display mdisp = getActivity().getWindowManager().getDefaultDisplay();
        Point outSize = new Point();
        mdisp.getSize(outSize);
        ObjectAnimator animX = ObjectAnimator.ofFloat(getView(), "x", outSize.x);
        ObjectAnimator animY = ObjectAnimator.ofFloat(getView(), "y", outSize.y);
        ObjectAnimator animScaleX = ObjectAnimator.ofFloat(getView(), "scaleX", 0);
        ObjectAnimator animScaleY = ObjectAnimator.ofFloat(getView(), "scaleY", 0);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(animX, animY, animScaleX, animScaleY);
        set.setDuration(mShortAnimationDuration);

        set.start();
        //mCurrentAnimator = set;
    }
    @Override
    public void onClick(View view) {
        Log.d("MemoZone","onClick");
        switch (view.getId()) {
            case R.id.editorSaveImage:
                if ((latitude == -9999.0) && (longitude == -9999.0)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                    dialog.setTitle("No Location Selected");
                    dialog.setCancelable(true);
                    dialog.setPositiveButton(
                            "View",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int id) {
                                    Dialog d = (Dialog) dialogInterface;
                                    d.cancel();
                                }
                            });
                    dialog.setMessage("Please indicate the location to save.");
                    dialog.show();
                    break;
                }
                String title = titleText.getText().toString();
                String message = bodyText.getText().toString();
                if (currentId == -1) {
                    Log.i("MemoZone", "Save New Record");
                    editorOperationCallback.onNewFileSaveClicked(title, imageAttachment, expiration, latitude, longitude, message, radius);
                } else {
                    Log.i("MemoZone", "Save Existing Record: " + message);
                    editorOperationCallback.onExistingFileSaveClicked(currentId, title, imageAttachment, expiration, latitude, longitude, message, radius);
                }
                break;
            case R.id.editorDeleteImage:
                animatedDelete();
                editorOperationCallback.onDeleteClicked();
                break;
            case R.id.editorMapsImage: {
                Log.d("MemoZone", "location Image clicked");
                Intent intent = new Intent(getContext(), MapActivity.class);
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                intent.putExtra("radius", radius);
                startActivityForResult(intent, LOCATION_REQUEST);
                break;
            }
            case R.id.editorTimeImage: {
                Log.d("MemoZone", "Timer Image clicked");
                Intent intent = new Intent(getContext(), TimeAndCalendarActivity.class);
                startActivityForResult(intent, EXPIRATION_DATE_REQUEST);
                break;
            }
            case R.id.editorCameraImage: {
                Intent intent = new Intent(getContext(), CameraActivity.class);
                if (imageAttachment != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    imageAttachment.compress(Bitmap.CompressFormat.PNG, 0, stream);
                    intent.putExtra("image", stream.toByteArray());
                }
                startActivityForResult(intent, PHOTO_REQUEST);
                break;
            }
            case R.id.editorHomeImageView: {
                if (editorOperationCallback != null) editorOperationCallback.onHomeClicked();
                break;
            }
        }
    }
}
