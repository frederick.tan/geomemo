package apps.crazy.potato.honey.memozone;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Definition: Main Activity
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class MainActivity
        extends Activity
        implements View.OnClickListener,
        LocationListener
{
    /** Static variables are defined here */
    /** This is used to get a result from the FileListActivity*/
    private static final int FILE_LIST_ACTION = 1;
    /** These are used to get result from Request Location Service Request*/
    private static final int LOCATION_PERMISSION_START = 11;
    private static final int LOCATION_PERMISSION_STOP = 12;
    /** This is used to get a result from DisplayMemoActivity */
    private static final int DISPLAY_MEMO_ACTION = 21;

    /** The minimum distance to change Updates in meters */
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    /** The minimum time between updates in milliseconds */
    private static final long MIN_TIME_BW_UPDATES = 60 * 1000;      // 1 minute
    /** Define global variables here */
    boolean isGPSEnabled;
    boolean isNetworkEnabled;
    private LocationManager mLocationManager;
    private DBAdapter mDBAdapter;
    private Map<String, Record> mRecordObjects;
    private Map<String, Record> mRecordsToService;
    AlertDialog.Builder mServiceMemoDialog;
    AlertDialog.Builder mTimeoutMemoDialog;
    private MediaPlayer mMediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /** Setup the custom title bar */
        setContentView(R.layout.activity_main);

        /** Setup the image view listeners*/
        ImageView newMemoImageView;
        ImageView savedMemosImageView;
        ImageView aboutImageView;

        newMemoImageView = (ImageView)(findViewById(R.id.newMemo));
        if (newMemoImageView != null) newMemoImageView.setOnClickListener(this);

        savedMemosImageView = (ImageView)(findViewById(R.id.savedMemo));
        if (savedMemosImageView != null) savedMemosImageView.setOnClickListener(this);

        aboutImageView = (ImageView)(findViewById(R.id.about));
        if (aboutImageView != null) aboutImageView.setOnClickListener(this);

        /** Setup the Location Manager and Location Services */
        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        /** Initialize DB related objects */
        mDBAdapter = new DBAdapter(this);
        mRecordObjects = new HashMap<>();
        mRecordsToService = new HashMap<>();

        /** Define the Dialogs we are going to need*/
        mServiceMemoDialog = new AlertDialog.Builder(this);
        mTimeoutMemoDialog = new AlertDialog.Builder(this);
        setupAlarm();

        Intent serviceIntent = new Intent(MainActivity.this, MemoZoneService.class);
        serviceIntent.putExtra("test", "test");
        startService(serviceIntent);
    }
    @Override
    protected void onResume() {
        super.onResume();
        readDB();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newMemo:
                runEditorActivity("");
                break;
            case R.id.savedMemo:
                runFileListActivity();
                break;
            case R.id.about:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
        }

    }
    @Override
    protected void onStart() {
        super.onStart();
        try {
            mLocationManager = (LocationManager)getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isGPSEnabled || isNetworkEnabled) {
                //this.canGetLocation = true;
                // First get location from Network Provider
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (isNetworkEnabled) {
                        mLocationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("MemoZone", "Network");
                    }
                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        mLocationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("MemoZone", "GPS Enabled");
                    }
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                            LOCATION_PERMISSION_START);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                            LOCATION_PERMISSION_STOP);
        } else {
            mLocationManager.removeUpdates(this);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0
                && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                switch (requestCode) {
                    case LOCATION_PERMISSION_START: {
                        if (isNetworkEnabled) {
                            mLocationManager.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            Log.d("MemoZone", "Network");
                        }
                        // if GPS Enabled get lat/long using GPS Services
                        if (isGPSEnabled) {
                            mLocationManager.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            Log.d("MemoZone", "GPS Enabled");
                        }
                        break;
                    }
                    case LOCATION_PERMISSION_STOP: {
                        mLocationManager.removeUpdates(this);
                        break;
                    }
                }
            } else {
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void runEditorActivity(String title) {
        Intent intent = new Intent(this, ReminderEditorSliderActivity.class);
        if (title.isEmpty()) {
            Log.d("MemoZone", "Running Editor Activity(e): " + title);
            intent.putExtra("isNewFlag", true);
        }
        else {
            Log.d("MemoZone", "Running Editor Activity: " + title);
            intent.putExtra("isNewFlag", false);
        }
        intent.putExtra("title", title);
        startActivity(intent);
    }
    private void runFileListActivity() {
        Intent intent = new Intent(this, FileListActivity.class);
        startActivityForResult(intent, FILE_LIST_ACTION);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        switch (requestCode) {
            case FILE_LIST_ACTION: {
                String action = data.getStringExtra("action");
                if (action.equals("open")) {
                    String title = data.getStringExtra("title");
                    if (!title.isEmpty())
                        runEditorActivity(title);
                } else if (action.equals("create")){
                    runEditorActivity("");
                } else if (action.equals("ok")) {
                    /** Do nothing */
                }
                break;
            }
            case DISPLAY_MEMO_ACTION: {
                String action = data.getStringExtra("action");
                Log.d("MemoZone", "DISPLAY_MEMO_ACTION");
                if (action.equals("delete")) {
                    Long id = data.getLongExtra("id", -1);
                    if (id > -1) {
                        mDBAdapter.open();
                        mDBAdapter.remove(id);
                        mDBAdapter.close();
                        readDB();
                        Toast.makeText(this, "Record removed: " + String.valueOf(id), Toast.LENGTH_SHORT).show();
                        Log.d("MemoZone", "Record removed: " + String.valueOf(id));
                    }
                } else if (action.equals("ok")) {
                    /** Do nothing */
                    Log.d("MemoZone", "Pressed ok");
                }
                break;
            }
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        Log.d("MemoZone", "Location Changed!!!");
        Double latitude = location.getLatitude();
        Double longitude = location.getLongitude();
        float[] result = new float[2];
        //mRecordsToService.clear();
        for (Map.Entry<String, Record> entry : mRecordObjects.entrySet())
        {
            Record record = entry.getValue();
            Location.distanceBetween(latitude, longitude, record.latitude, record.longitude, result);
            Log.d("MemoZone", "Distance from " + record.title + ": " + String.valueOf(result[0]));
            if (result[0] < record.radius) {
                /** If it is already added in the service list, continue*/
                if (mRecordsToService.containsKey(record.title)) continue;
                mRecordsToService.put(record.title, record);
                Log.d("MemoZone", "Servicing record: " + record.title);
            } else {
                /** If the record is no longer valid (out of range), but is already in the list, remove it*/
                if (mRecordsToService.containsValue(record)) mRecordsToService.remove(record.title);
                Log.d("MemoZone", "Removing record from Servicing List: " + record.title);
            }
        }
        if (mRecordsToService.isEmpty()) return;
        for (Map.Entry<String, Record> entry : mRecordsToService.entrySet()) {
            final Record record = entry.getValue();

            if (mServiceMemoDialog != null) {
                /** Define the consistent infos */
                mServiceMemoDialog.setTitle("New Memo triggered");
                mServiceMemoDialog.setCancelable(true);
                mServiceMemoDialog.setPositiveButton(
                        "View",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int id) {
                                Dialog dialog  = (Dialog) dialogInterface;
                                Context context = dialog.getContext();
                                Log.d("MemoZone", "Sending: " + String.valueOf(record.id));
                                Intent intent = new Intent(context, DisplayMemoActivity.class);
                                intent.putExtra("id", record.id);
                                intent.putExtra("title", record.title);
                                intent.putExtra("expiration", record.expirationDate);
                                intent.putExtra("message", record.message);
                                intent.putExtra("image", getByteArray(record.attachedImage));
                                intent.putExtra("dateCreated", record.dateCreated);
                                intent.putExtra("timeCreated", record.timeCreated);
                                intent.putExtra("radius", record.radius);
                                intent.putExtra("geolocx", record.latitude);
                                intent.putExtra("geolocy", record.longitude);
                                startActivityForResult(intent, DISPLAY_MEMO_ACTION);
                                mMediaPlayer.reset();
                                dialog.cancel();
                            }
                        });
                mServiceMemoDialog.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                mMediaPlayer.reset();
                            }
                        });
                mServiceMemoDialog.setMessage(record.title);
                mServiceMemoDialog.create();
                mServiceMemoDialog.show();
                mMediaPlayer.start();
            }
        }
        readDB();
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }
    @Override
    public void onProviderEnabled(String provider) {

    }
    @Override
    public void onProviderDisabled(String provider) {

    }
    /** Convert a bitmap to byte array, which is needed in our DB*/
    public byte[] getByteArray(Bitmap bitmap) {
        if (bitmap == null) return null;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
    public void readDB() {
        mRecordObjects.clear();
        //Open database
        mDBAdapter.open();
        //Retrieve all records
        Cursor c = mDBAdapter.getAllNames();
        while(c.moveToNext())
        {
            Long id = c.getLong(0);
            String title = c.getString(1);
            String dateCreated = c.getString(2);
            String timeCreated = c.getString(3);
            byte[] imageAttachment = c.getBlob(4);
            String expirationDate = c.getString(5);
            Double geolocX = c.getDouble(6);
            Double geolocY = c.getDouble(7);
            String message = c.getString(8);
            Double radius = c.getDouble(9);
            Bitmap image = null;
            if (imageAttachment != null)
                image = BitmapFactory.decodeByteArray(imageAttachment, 0, imageAttachment.length);
            Record object = new Record(id, title, dateCreated, timeCreated, image, expirationDate, geolocX, geolocY, message, radius);
            mRecordObjects.put(title, object);
        }
        mDBAdapter.close();
    }
    public void setupAlarm() {
        /** Use the system alarm setting */
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if ((alert == null) || (RingtoneManager.getRingtone(this, alert) == null)) {
            /** If it is null, use the default alarm */
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if ((alert == null) || (RingtoneManager.getRingtone(this, alert) == null)) {
                /** If still null, use the next default alarm */
                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        mMediaPlayer = MediaPlayer.create(getApplicationContext(), alert);
    }
}
