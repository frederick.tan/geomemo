package apps.crazy.potato.honey.memozone;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Definition: Google Map Activity
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class ReminderEditorSliderActivity
        extends FragmentActivity
        implements ReminderEditorFragment.EditorOperation,
        View.OnClickListener {
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ArrayList<String> fileNameList;
    private boolean isNewNote;
    private ImageView deleteImage;
    private DBAdapter dbAdapter;
    private ImageView noteView;
    private Map<String, Record> recordObjects;
    private Record currentRecord;
    private String currentFileName;
    private ViewPager mPager;
    private EditText titleEditText;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.slider_activity);
        //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.editor_title_bar);

        //titleEditText = (EditText) findViewById(R.id.editorTitle);
        dbAdapter = new DBAdapter(this);
        if (fileNameList == null) fileNameList = new ArrayList<String>();

        Intent intent = getIntent();
        isNewNote = intent.getBooleanExtra("isNewFlag", true);
        //Integer id = intent.getIntExtra("id", -1);
        String title = intent.getStringExtra("title");
        //String expirationDate = intent.getStringExtra("expiration");
        //String message = intent.getStringExtra("message");
        //String attachmentName = intent.getStringExtra("attachmentName");
        //String dateCreated = intent.getStringExtra("dateCreated");
        //String timeCreated = intent.getStringExtra("timeCreated");
        //Double radius = intent.getDoubleExtra("radius", -1.0);
        //Double geolocx = intent.getDoubleExtra("geolocx", -1.0);
        //Double geolocy = intent.getDoubleExtra("geolocy", -1.0);
        Log.d("MemoZone", "Received: " + title);
        recordObjects = new HashMap<>();
        readDB();
        if (recordObjects != null) currentRecord = recordObjects.get(title);
        if (currentRecord != null) {
            Log.d("MemoZone", "Record found!" + currentRecord.id);
            if (titleEditText != null) titleEditText.setText(currentRecord.title);
            currentFileName = currentRecord.title;
            Log.d("MemoZone", "title : " + title);
        } else {
            currentFileName = "Memo01";
            currentFileName = checkFileNameAndAutoRename(currentFileName);
            //if (titleEditText != null) {
            //    titleEditText.setText(currentFileName);
                Log.d("MemoZone", "title new: " + currentFileName);
            //}
        }

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new WordPerfectSliderAdapter(getSupportFragmentManager());
        Log.d("MemoZone", "Search: " + currentFileName);
        int currentItem = fileNameList.indexOf(currentFileName);
        if (currentItem == -1) {
            Log.d("MemoZone", "Not found: " + currentFileName);
            isNewNote = true;
            currentItem = fileNameList.size();
        }
        Log.i("MemoZone", "This is the current item: " + String.valueOf(currentItem));
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(currentItem);
    }
    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            if (mPagerAdapter != null) mPagerAdapter.notifyDataSetChanged();
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }
    @Override
    public void onClick(View view) {
        //switch (view.getId()) {
        //case R.id.editorDeleteImage:
        //    String filename = fileNameList.get(mPager.getCurrentItem());
        //    Log.i("MemoZone", "delete (actual): " + filename);
        //    break;
        //}
    }
    public void readDB() {
        fileNameList.clear();
        recordObjects.clear();
        //Open database
        dbAdapter.open();
        //Retrieve all records
        Cursor c = dbAdapter.getAllNames();
        while (c.moveToNext()) {
            Long id = c.getLong(0);
            String title = c.getString(1);
            String dateCreated = c.getString(2);
            String timeCreated = c.getString(3);
            byte[] imageAttachment = c.getBlob(4);
            String expirationDate = c.getString(5);
            Double geolocX = c.getDouble(6);
            Double geolocY = c.getDouble(7);
            String message = c.getString(8);
            Double radius = c.getDouble(9);
            Bitmap image = null;
            if (imageAttachment != null)
                image = BitmapFactory.decodeByteArray(imageAttachment, 0, imageAttachment.length);
            Record object = new Record(id, title, dateCreated, timeCreated, image, expirationDate, geolocX, geolocY, message, radius);
            recordObjects.put(title, object);
            fileNameList.add(title);
            Log.d("MemoZone", "Adding: " + title);
        }
        dbAdapter.close();
    }
    public void onDeleteClicked() {
        // Check if the condition for calling the onDeleteClicked function is valid
        if (fileNameList.size() == 0 && !isNewNote) return;
        String filename, displayText;
        int item = mPager.getCurrentItem();
        if (item >= fileNameList.size()) filename = "";
        else filename = (fileNameList.size() > 0) ? fileNameList.get(item) : "";
        if (filename.equals("")) {
            isNewNote = false;
            displayText = "Canceled";
        } else {
            if (recordObjects != null) {
                Record object = recordObjects.get(filename);
                if (object != null) {
                    dbAdapter.open();
                    if (dbAdapter.remove(object.id)) recordObjects.remove(filename);
                    dbAdapter.close();
                }
            }
            int currentIndex = mPager.getCurrentItem();
            View view = mPager.findViewWithTag(currentIndex);
            if (view != null) {
                view.invalidate();
                mPager.removeView(view);
            }
            fileNameList.remove(currentIndex);
            mPagerAdapter.notifyDataSetChanged();
            if (currentIndex > 0) {
                mPager.setCurrentItem(currentIndex - 1);
            } else if (currentIndex < fileNameList.size() - 1) {
                mPager.setCurrentItem(currentIndex + 1);
            } else {
                mPager.setCurrentItem(-1);
            }
            //fileManager.serializeFileList(getApplicationContext(), fileNameList);
            displayText = "File deleted";
        }
        mPagerAdapter.notifyDataSetChanged();
        if (fileNameList.size() <= 0) this.finish();
        Toast.makeText(getApplicationContext(), displayText, Toast.LENGTH_LONG).show();
    }
    public void onHomeClicked() {
        finish();
    }
    public long onNewFileSaveClicked(String title, Bitmap image, String expirationDate, Double geoLocationX, Double getGeoLocationY, String message, Double radius) {
        long id = -1;
        String time = DateFormat.getTimeInstance().format(new Date(0));
        String date = DateFormat.getDateInstance().format(new Date(0));
        title = checkFileNameAndAutoRename(title);
        if (dbAdapter != null) {
            dbAdapter.open();
            id = dbAdapter.insert(title, getByteArray(image), date, time, expirationDate, geoLocationX, getGeoLocationY, message, radius);
            Toast.makeText(getApplicationContext(), "Record inserted", Toast.LENGTH_LONG).show();
            dbAdapter.close();
        }
        isNewNote = false;
        readDB();
        mPagerAdapter.notifyDataSetChanged();
        return id;
    }
    public void onExistingFileSaveClicked(Long id, String title, Bitmap image, String expirationDate, Double geoLocationX, Double getGeoLocationY, String message, Double radius) {
        String time = DateFormat.getTimeInstance().format(new Date(0));
        String date = DateFormat.getDateInstance().format(new Date(0));
        if (dbAdapter != null) {
            dbAdapter.open();
            Long retValue = dbAdapter.update(id, title, getByteArray(image), expirationDate, date, time, geoLocationX, getGeoLocationY, message, radius);
            Toast.makeText(getApplicationContext(), "Record changed: " + String.valueOf(retValue), Toast.LENGTH_LONG).show();
            dbAdapter.close();
        }
        readDB();
        mPagerAdapter.notifyDataSetChanged();
    }
    /** Convert a bitmap to byte array, which is needed in our DB*/
    public byte[] getByteArray(Bitmap bitmap) {
        if (bitmap == null) return null;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
    public String checkFileNameAndAutoRename(String currentFileName) {
        String newFilename = currentFileName;
        int postfix = 2;
        while (true) {
            if (!fileNameExists(newFilename)) break;
            newFilename = currentFileName + "_" + postfix;
            postfix++;
        }
        return newFilename;
    }
    public boolean fileNameExists(String fileName) {
        for (String existingFileName : fileNameList) {
            if (fileName.equals(existingFileName)) {
                return true;
            }
        }
        return false;
    }
    public class WordPerfectSliderAdapter extends FragmentStatePagerAdapter {
        private boolean resyncView = false;

        public WordPerfectSliderAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ReminderEditorFragment editor = new ReminderEditorFragment();
            if (isNewNote && position == fileNameList.size()) {
                //editor.setCurrentFile("");
                //editor.setCurrentData("");
                //editor.setCurrentTitle("");
                Bundle args = new Bundle();
                args.putLong("id", -1);
                args.putString("title", currentFileName);
                args.putString("data", "");
                editor.setArguments(args);
                Log.d("MemoZone", "putting(n): " + currentFileName);
            } else {
                String filename = fileNameList.get(position);
                Record record = recordObjects.get(filename);
                Bundle args = new Bundle();
                args.putLong("id", record.id);
                args.putString("title", record.title);
                args.putString("data", record.message);
                args.putDouble("radius", record.radius);
                args.putDouble("latitude", record.latitude);
                args.putDouble("longitude", record.longitude);
                args.putByteArray("image", getByteArray(record.attachedImage));
                editor.setArguments(args);
                View view = editor.getView();
                if (view != null) view.setTag(position);
                Log.d("MemoZone", "putting(o): " + record.title);
            }
            Log.d("MemoZone", "position: " + position);
            return editor;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object object = super.instantiateItem(container, position);
            Fragment fragment = (Fragment)object;
            View view = fragment.getView();
            if (view != null) {
                view.setTag(position);
                Log.i("MemoZone", "Tagged! " + String.valueOf(position));
            }
            //container.setTag(position);
            return object;
        }

        @Override
        public int getCount() {
            if (fileNameList != null)
                if (isNewNote) return fileNameList.size() + 1;
                else return fileNameList.size();
            else
                return 0;
        }

        @Override
        public int getItemPosition(Object object) {
            //return POSITION_NONE;

            if (fileNameList.contains(object)) {
                return fileNameList.indexOf(object);
            } else {
                return POSITION_NONE;
            }
        }
        public void resetView() {
            resyncView = true;
            notifyDataSetChanged();
        }
    }
}
