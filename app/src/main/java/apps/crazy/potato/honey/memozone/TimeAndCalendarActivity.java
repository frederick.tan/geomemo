package apps.crazy.potato.honey.memozone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Definition: Calendar Activity
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class TimeAndCalendarActivity
        extends Activity
        implements View.OnClickListener,
        DatePicker.OnDateChangedListener
{
    DatePicker datePicker;
    Button buttonDone;
    TextView dateSelectedTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.time_and_calendar);
        datePicker = (DatePicker)findViewById(R.id.datePicker);
        if (datePicker != null) {
            //datePicker.setMinDate();
            Calendar calendar = Calendar.getInstance();
            datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), this);
        }
        buttonDone = (Button)findViewById(R.id.buttonDateSelectionDone);
        if (buttonDone != null) {
            buttonDone.setOnClickListener(this);
        }
        dateSelectedTextView = (TextView)findViewById(R.id.textViewDateSelected);
        if (dateSelectedTextView != null) {
            String date = DateFormat.getDateInstance().format(new Date());
            dateSelectedTextView.setText(date);
        }
    }
    @Override
    public void onClick(View v) {
        Integer id = v.getId();
        switch (id) {
            case R.id.buttonDateSelectionDone:
                Intent intent = new Intent();
                intent.putExtra("expiration", dateSelectedTextView.getText().toString());
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
        }
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Integer id = view.getId();
        if (id == R.id.datePicker) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);
            String date = DateFormat.getDateInstance().format(calendar.getTime());
            dateSelectedTextView.setText(date);
        }
    }
}