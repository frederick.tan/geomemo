package apps.crazy.potato.honey.memozone;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Definition: Display a specific Memo
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class DisplayMemoActivity
        extends Activity
        implements View.OnClickListener
{
    private Long mId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_memo);
        Log.d("MemoZone", "DisplayMemoActivity::onCreate");
        Intent intent = getIntent();
        mId = intent.getLongExtra("id", -1);
        String title = intent.getStringExtra("title");
        String expirationDate = intent.getStringExtra("expiration");
        String message = intent.getStringExtra("message");
        Bitmap bitmap = null;
        if (intent.hasExtra("image")) {
            bitmap = BitmapFactory.decodeByteArray(
                    intent.getByteArrayExtra("image"), 0, intent.getByteArrayExtra("image").length);
        }
        String dateCreated = intent.getStringExtra("dateCreated");
        String timeCreated = intent.getStringExtra("timeCreated");
        Double radius = intent.getDoubleExtra("radius", -1.0);
        Double latitude = intent.getDoubleExtra("geolocx", -1.0);
        Double longitude = intent.getDoubleExtra("geolocy", -1.0);
        TextView titleTextView = (TextView)findViewById(R.id.displayMemoTitle);
        TextView dataTextView = (TextView)findViewById(R.id.displayMemoTextView);
        ImageView imageView = (ImageView)findViewById(R.id.displayMemoImageView);
        if (titleTextView != null) {
            titleTextView.setText(title);
        }
        if (dataTextView != null) {
            String displayText = "Latitude: " + String.valueOf(latitude) + "\n";
            displayText += "Longitude: " + String.valueOf(longitude) + "\n";
            displayText += "Area radius: " + String.valueOf(radius) + "meters\n";
            displayText += "Date\\Time Created: " + dateCreated + " " + timeCreated + "\n";
            displayText += "Expiring On: " + expirationDate + "\n";
            displayText += "Message:\n" + message;
            dataTextView.setText(displayText);
        }
        if (imageView != null) {
            if (bitmap == null) {
                imageView.setImageResource(R.drawable.imageplaceholder);
            } else {
                imageView.setImageBitmap(bitmap);
            }
        }
        ImageView okImageView = (ImageView)findViewById(R.id.displayMemoOk);
        ImageView deleteImageView = (ImageView)findViewById(R.id.displayMemoDelete);
        if (okImageView != null) {
            okImageView.setOnClickListener(this);
        }
        if (deleteImageView != null) {
            deleteImageView.setOnClickListener(this);
        }
    }
    @Override
    public void onClick(View v) {
        Integer id = v.getId();
        switch (id) {
            case R.id.displayMemoOk: {
                Intent intent = new Intent();
                intent.putExtra("action", "ok");
                setResult(Activity.RESULT_OK, intent);
                Log.d("MemoZone", "Ok");
                finish();
                break;
            }
            case R.id.displayMemoDelete: {
                Intent intent = new Intent();
                intent.putExtra("action", "delete");
                intent.putExtra("id", mId);
                setResult(Activity.RESULT_OK, intent);
                Log.d("MemoZone", "displayMemoDelete: " + String.valueOf(mId));
                finish();
                break;
            }
        }
    }
}
