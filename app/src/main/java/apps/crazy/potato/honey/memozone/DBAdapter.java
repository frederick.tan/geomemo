package apps.crazy.potato.honey.memozone;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Definition: Defines the necessary DB APIs
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class DBAdapter {
    final Context context;
    SQLiteHelper helper;
    SQLiteDatabase db;

    public DBAdapter(Context ctx) {
        context = ctx;
        helper = new SQLiteHelper(context);
    }
    // Open the database
    public DBAdapter open()
    {
        try {
            db  = helper.getWritableDatabase();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return this;
    }
    //Close the database
    public void close()
    {
        helper.close();
    }
    //Insert into the database
    public long insert(String title, byte[] image, String dateCreated, String timeCreated, String expirationDate, Double geolocx, Double geolocy, String message, Double radius)
    {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteHelper.COLUMN_BITMAP, image);
            contentValues.put(SQLiteHelper.COLUMN_TITLE, title);
            contentValues.put(SQLiteHelper.COLUMN_DATE_CREATED, dateCreated);
            contentValues.put(SQLiteHelper.COLUMN_TIME_CREATED, timeCreated);
            contentValues.put(SQLiteHelper.COLUMN_EXPIRATION_DATE, expirationDate);
            contentValues.put(SQLiteHelper.COLUMN_GEOLOC_X, geolocx);
            contentValues.put(SQLiteHelper.COLUMN_GEOLOC_Y, geolocy);
            contentValues.put(SQLiteHelper.COLUMN_MESSAGE, message);
            contentValues.put(SQLiteHelper.COLUMN_COVERAGE_RADIUS, radius);
            return db.insert(SQLiteHelper.TABLE_NAME, SQLiteHelper.COLUMN_ID, contentValues);
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    //Update the database
    public long update(Long id, String title, byte[] image, String dateCreated, String timeCreated, String expirationDate, Double geolocx, Double geolocy, String message, Double radius)
    {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(SQLiteHelper.COLUMN_BITMAP, image);
            contentValues.put(SQLiteHelper.COLUMN_TITLE, title);
            contentValues.put(SQLiteHelper.COLUMN_DATE_CREATED, dateCreated);
            contentValues.put(SQLiteHelper.COLUMN_TIME_CREATED, timeCreated);
            contentValues.put(SQLiteHelper.COLUMN_EXPIRATION_DATE, expirationDate);
            contentValues.put(SQLiteHelper.COLUMN_GEOLOC_X, geolocx);
            contentValues.put(SQLiteHelper.COLUMN_GEOLOC_Y, geolocy);
            contentValues.put(SQLiteHelper.COLUMN_MESSAGE, message);
            contentValues.put(SQLiteHelper.COLUMN_COVERAGE_RADIUS, radius);
            String[] whereArgs = {String.valueOf(id)};
            return db.update(SQLiteHelper.TABLE_NAME, contentValues, SQLiteHelper.COLUMN_ID +"=?", whereArgs);
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    //DeleteEntry
    public boolean remove(long columnId){
        return db.delete(SQLiteHelper.TABLE_NAME, SQLiteHelper.COLUMN_ID+ "=" + columnId, null) > 0;
    }
    //Query
    public String getMessage(long columnId){
        String message = new String();
        Cursor CURSOR = db.query(SQLiteHelper.TABLE_NAME, new String[]{}, SQLiteHelper.COLUMN_ID + "=" + columnId, null, null, null, null);
        if (CURSOR.getCount()>0){
            CURSOR.moveToFirst();
            message = CURSOR.getString(8);
            return message;
        }
        CURSOR.close();
        return message;
    }
    //Get All Values
    public Cursor getAllNames()
    {
        String[] columns={SQLiteHelper.COLUMN_ID, SQLiteHelper.COLUMN_TITLE, SQLiteHelper.COLUMN_DATE_CREATED, SQLiteHelper.COLUMN_TIME_CREATED,
                SQLiteHelper.COLUMN_BITMAP, SQLiteHelper.COLUMN_EXPIRATION_DATE, SQLiteHelper.COLUMN_GEOLOC_X,
                SQLiteHelper.COLUMN_GEOLOC_Y, SQLiteHelper.COLUMN_MESSAGE, SQLiteHelper.COLUMN_COVERAGE_RADIUS};
        return db.query(SQLiteHelper.TABLE_NAME, columns, null, null, null, null, null);
    }
}
