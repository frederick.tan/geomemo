package apps.crazy.potato.honey.memozone;

import android.app.IntentService;
import android.content.Intent;
import android.location.LocationManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Definition: The IntentService handler Class
 * Date Created: 06/06/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class MemoZoneService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public MemoZoneService() {
        super(MemoZoneService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent.getExtras().containsKey("test")) {
            Log.d("MemoZone", "We were invoked");
        }
        if (!intent.getExtras().containsKey(LocationManager.KEY_LOCATION_CHANGED)) {
            /** We are not interested in this Intent */
            return;
        }
        Intent notificationClickIntent = new Intent(this, MainActivity.class);

        Log.d("MemoZone", "onHandleIntent");
    }
}
