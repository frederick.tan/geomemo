package apps.crazy.potato.honey.memozone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Definition: Defines the File List Adapter class
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class FileListCustomAdapter
        extends BaseAdapter
{
    /** Declaration of all local variables */
    private final Context context;
    private ArrayList<String> fileNameList;
    private View.OnClickListener onClickListener;

    /** Interface */
    public interface FileListAction {
        void onFileNameListChanged(ArrayList<String> newList);
    }
    private FileListAction fileListActivityCb;

    /** The parent class/fragment should implement the FileListAction Interface and the OnClickListener  */
    /** The FileListAction Interface is for event notification when there is an element added or deleted */
    /** so that the parent fragment can save the new fileNameList and to update the view                 */
    public FileListCustomAdapter(Context context, ArrayList<String> values, FileListAction listFragment, View.OnClickListener listener) {
        this.context = context;
        this.fileNameList = values;
        onClickListener = listener;
        fileListActivityCb = listFragment;
        if (fileListActivityCb == null)
            throw new ClassCastException("The parent class must implement FileListCustomAdapter.FileListAction interface.");
        if (onClickListener == null)
            throw new ClassCastException("The parent class must implement OnClickListener interface.");
    }
    private class Holder
    {
        ImageView imageView = null;
        TextView textView = null;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Holder holder = new Holder();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.file_name_list_text_view, null);
            holder.imageView = (ImageView) convertView.findViewById(R.id.deleteFileNameImageView);
            holder.textView = (TextView) convertView.findViewById(R.id.fileNameTextView);

            if (holder.imageView != null) {
                holder.imageView.setClickable(true);
                holder.imageView.setEnabled(true);
                holder.imageView.setOnClickListener(onClickListener);
                // Pass the textView as tag so we can still access the string content in the parent fragment
                holder.imageView.setTag(holder.textView);
            }
            if (holder.textView != null) {
                holder.textView.setClickable(true);
                holder.textView.setEnabled(true);
                holder.textView.setOnClickListener(onClickListener);
                holder.textView.setTag(holder.textView);
            }
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        if (holder.imageView != null) holder.imageView.setVisibility(View.VISIBLE);
        if (holder.textView != null) {
            holder.textView.setText(fileNameList.get(position));
            holder.textView.setTag(fileNameList.get(position));
        }
        return convertView;
    }
    @Override
    public int getCount()
    {
        if (fileNameList == null) return 0;
        return fileNameList.size();
    }
    @Override
    public Object getItem(int position) {
        return fileNameList.get(position);
    }
    public long getItemId(int position)
    {
        return position;
    }
    public void dataChanged(ArrayList<String> newList) {
        fileNameList = newList;
        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }
    public void deleteFileName(String fileName) {
        fileNameList.remove(fileName);
        notifyDataSetChanged();
        fileListActivityCb.onFileNameListChanged(fileNameList);
    }
}
