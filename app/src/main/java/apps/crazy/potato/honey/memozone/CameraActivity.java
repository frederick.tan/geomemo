package apps.crazy.potato.honey.memozone;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;

/**
 * Definition: Class handling the PhotoAttachment
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class CameraActivity
        extends Activity
        implements View.OnClickListener
{
    private static final int REQUEST_IMAGE_CAPTURE  = 1;
    private static final int CHOOSE_FROM_GALLERY = 2;
    private Bitmap image;
    private Bitmap originalImage;
    private ImageView selectedPhotoImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_activity);
        Intent intent = getIntent();
        if (intent.hasExtra("image")) {
            image = BitmapFactory.decodeByteArray(
                    intent.getByteArrayExtra("image"), 0, intent.getByteArrayExtra("image").length);
            originalImage = image;
        }
        ImageView okImageView = (ImageView)findViewById(R.id.cameraOkImageView);
        if (okImageView != null) {
            okImageView.setOnClickListener(this);
        }
        ImageView cancelImageView = (ImageView)findViewById(R.id.cameraCancelImageView);
        if (cancelImageView != null) {
            cancelImageView.setOnClickListener(this);
        }

        selectedPhotoImageView = (ImageView)findViewById(R.id.cameraSelectedPhotoImageView);
        if (selectedPhotoImageView != null) {
            selectedPhotoImageView.setOnClickListener(this);
            if (image == null) {
                image = BitmapFactory.decodeResource(getResources(), R.drawable.imageplaceholder);
            }
            selectedPhotoImageView.setImageBitmap(image);
        }

    }
    @Override
    public void onClick(View v) {
        Integer id = v.getId();
        switch (id) {
            case R.id.cameraOkImageView: {
                Log.d("MemoZone", "cameraOkImageView");
                Intent intent = new Intent();
                intent.putExtra("image", compressToByteArray(image));
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
            }
            case R.id.cameraCancelImageView: {
                Log.d("MemoZone", "cameraOkImageView");
                if (originalImage != null) {
                    image = originalImage;
                } else {
                    image = BitmapFactory.decodeResource(getResources(), R.drawable.imageplaceholder);
                }
                Intent intent = new Intent();
                intent.putExtra("image", compressToByteArray(image));
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
            }
            case R.id.cameraSelectedPhotoImageView: {
                dispatchTakePictureIntent();
                break;
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE: {
                Log.d("MemoZone", "REQUEST_IMAGE_CAPTURE");
                Bundle extras = data.getExtras();
                image = (Bitmap)extras.get("data");
                Log.d("MemoZone", "Still here");
                selectedPhotoImageView.setImageBitmap(image);
                break;
            }
            case CHOOSE_FROM_GALLERY: {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();
                byte[] byteArrayImage = compressToByteArray(BitmapFactory.decodeFile(filePath));
                image = BitmapFactory.decodeByteArray(byteArrayImage, 0, byteArrayImage.length);
                image = Bitmap.createScaledBitmap(image, 200, 200, true);
                selectedPhotoImageView.setImageBitmap(image);
                break;
            }
        }

    }
    public byte[] compressToByteArray(Bitmap bitmap) {
        Bitmap compressedBitmap = bitmap;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        compressedBitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }
    private void dispatchTakePictureIntent() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Attach File");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                    }
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, CHOOSE_FROM_GALLERY);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

}
