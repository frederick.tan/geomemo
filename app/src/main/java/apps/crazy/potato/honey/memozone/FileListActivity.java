package apps.crazy.potato.honey.memozone;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by frede_000 on 25/04/2016.
 */
public class FileListActivity
        extends Activity
        implements View.OnClickListener,
        FileListCustomAdapter.FileListAction
{
    /** Declaration of all local variables */
    private ArrayList<String> fileNameList;
    private Map<String, Record> recordObjects;
    private DBAdapter dbAdapter;
    private FileListCustomAdapter fileListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_list_fragment);
        ListView listView;
        recordObjects = new HashMap<>();
        if (fileNameList == null) fileNameList = new ArrayList<String>();
        dbAdapter = new DBAdapter(this);
        readDB();
        fileListAdapter =
                new FileListCustomAdapter(this, fileNameList, this, this);
        listView = (ListView) (findViewById(R.id.fileListView));
        if (listView != null) {
            listView.setAdapter(fileListAdapter);
        }
        ImageView okImageView = (ImageView)findViewById(R.id.fileListOkImageView);
        ImageView newImageView = (ImageView)findViewById(R.id.fileListNewImageView);
        if (okImageView != null) {
            okImageView.setOnClickListener(this);
        }
        if (newImageView != null) {
            newImageView.setOnClickListener(this);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        readDB();
        fileListAdapter.dataChanged(fileNameList);
    }
    public void readDB() {
        fileNameList.clear();
        recordObjects.clear();
        //Open database
        dbAdapter.open();
        //Retrieve all records
        Cursor c = dbAdapter.getAllNames();
        while(c.moveToNext())
        {
            Long id = c.getLong(0);
            String title = c.getString(1);
            String dateCreated = c.getString(2);
            String timeCreated = c.getString(3);
            byte[] imageAttachment = c.getBlob(4);
            String expirationDate = c.getString(5);
            Double geolocX = c.getDouble(6);
            Double geolocY = c.getDouble(7);
            String message = c.getString(8);
            Double radius = c.getDouble(9);
            Bitmap image = null;
            if (imageAttachment != null)
                image = BitmapFactory.decodeByteArray(imageAttachment, 0, imageAttachment.length);
            Record object = new Record(id, title, dateCreated, timeCreated, image, expirationDate, geolocX, geolocY, message, radius);
            recordObjects.put(title, object);
            fileNameList.add(title);
        }
        dbAdapter.close();
    }
    public void onFileNameListChanged(ArrayList<String> newList) {
        fileNameList = newList;
    }
    public void onClick(View v) {
        Integer id = v.getId();
        switch (id) {
            case R.id.deleteFileNameImageView: {
                TextView tv = (TextView) v.getTag();
                String fileName = (String) tv.getTag();
                fileListAdapter.deleteFileName(fileName);
                dbAdapter.open();
                Record object = recordObjects.get(fileName);
                if (object != null) {
                    if (dbAdapter.remove(object.id) == true) recordObjects.remove(fileName);
                }
                break;
            }
            case R.id.fileNameTextView: {
                String fileName = (String) v.getTag();
                Intent intent = new Intent();
                intent.putExtra("title", fileName);
                intent.putExtra("action", "open");
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
            }
            case R.id.fileListNewImageView: {
                Intent intent = new Intent();
                intent.putExtra("action", "create");
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
            }
            case R.id.fileListOkImageView: {
                Intent intent = new Intent();
                intent.putExtra("action", "ok");
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
            }
        }
    }
}
