package apps.crazy.potato.honey.memozone;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by frede_000 on 30/05/2016.
 */
public class SQLiteHelper extends SQLiteOpenHelper {
    //COLUMNS
    public static final String COLUMN_ID="ID";
    public static final String COLUMN_TITLE="TITLE";
    public static final String COLUMN_DATE_CREATED="DATECREATED";
    public static final String COLUMN_TIME_CREATED="TIMECREATED";
    public static final String COLUMN_BITMAP = "BITMAP";
    public static final String COLUMN_EXPIRATION_DATE = "DATE";
    public static final String COLUMN_GEOLOC_X = "GEOLOCX";
    public static final String COLUMN_GEOLOC_Y = "GEOLOCY";
    public static final String COLUMN_MESSAGE = "MESSAGE";
    public static final String COLUMN_COVERAGE_RADIUS = "RADIUS";

    //DB PROPERTIES
    static final String DATABASE_NAME="PersonalAssistant.db.test.v4";
    static final String TABLE_NAME="REMINDER";
    static final int DATABASE_VERSION='1';
    static final String CREATE_TABLE = "create table " + TABLE_NAME + " ( "
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_TITLE + " VARCHAR,"
            + COLUMN_DATE_CREATED + " VARCHAR,"
            + COLUMN_TIME_CREATED + " VARCHAR,"
            + COLUMN_BITMAP + " BLOB, "
            + COLUMN_EXPIRATION_DATE + " VARCHAR, "
            + COLUMN_GEOLOC_X + " DOUBLE, "
            + COLUMN_GEOLOC_Y + " DOUBLE, "
            + COLUMN_MESSAGE + " VARCHAR, "
            + COLUMN_COVERAGE_RADIUS + " DOUBLE);";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
