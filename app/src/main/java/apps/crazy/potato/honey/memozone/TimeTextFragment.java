package apps.crazy.potato.honey.memozone;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by frede_000 on 30/05/2016.
 */
public class TimeTextFragment extends Fragment
{
    View.OnClickListener onClickListener;
    TextView timeTextView;
    Calendar rightNow = Calendar.getInstance();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("personal", "timeTextFragment::onCreateView 1");
        View view = inflater.inflate(R.layout.time_text, container, false);
        Log.d("personal", "timeTextFragment::onCreateView 2");
        String timeNow = rightNow.get(Calendar.HOUR_OF_DAY) + ":";
        int minute = rightNow.get(Calendar.MINUTE);
        if (minute < 10) timeNow += "0";
        timeNow += minute;
        Log.d("personal", "timeTextFragment::onCreateView 3");
        timeTextView = (TextView)view.findViewById(R.id.timeSelected);
        Log.d("personal", timeNow);
        if (timeTextView != null) {
            Log.d("personal", "timeTextView not null");
            timeTextView.setClickable(true);
            timeTextView.setText(timeNow);
            timeTextView.setEnabled(true);
            if (onClickListener != null)
                timeTextView.setOnClickListener(onClickListener);
        }
        return view;
    }
    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        Activity activity = null;
        if (ctx instanceof Activity) {
            activity = (Activity) ctx;
        }
        try {
            onClickListener = (View.OnClickListener)activity;
            if (timeTextView != null)
                timeTextView.setOnClickListener(onClickListener);
        } catch (ClassCastException e) {
            throw new ClassCastException("The activity " + activity.toString() + " must implement View.OnClickListener interface.");
        }
    }
}
