package apps.crazy.potato.honey.memozone;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Definition: Google Map Activity
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class MapActivity extends AppCompatActivity
        implements
        GoogleMap.OnMyLocationButtonClickListener,
        OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        LocationListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMarkerDragListener,
        SeekBar.OnSeekBarChangeListener,
        GoogleMap.OnInfoWindowLongClickListener,
        GoogleMap.OnInfoWindowCloseListener,
        View.OnClickListener
{
    private LocationManager mLocationManager;
    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;
    private static final int MAX_RADIUS = 10000;    /**10 KM*/
    private Marker mSelectedMarker;
    private Circle mCircle;
    private EditText mLatitudeText;
    private EditText mLongitudeText;
    private TextView mRadiusText;
    private SeekBar mRadiusSeekBar;
    /** The distance, in meters, which will trigger the event */
    /** Right now this is fixed to 1km                        */
    Double mRadius = 1000.0;
    private LatLng mOriginalLatLng;

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        Intent intent = getIntent();

        Double latitude = intent.getDoubleExtra("latitude", -9999.0);
        Double longitude = intent.getDoubleExtra("longitude", -9999.0);
        mRadius = intent.getDoubleExtra("radius", -9999.0);
        if (latitude != -9999 && longitude != -9999) {
            mOriginalLatLng = new LatLng(latitude, longitude);
        }
        if (mRadius < 0) {
            /** Set to default, which is 1KM*/
            mRadius = 1000.0;
        }

        mLatitudeText = (EditText)(findViewById(R.id.textViewSelectedLatitude));
        mLongitudeText = (EditText)(findViewById(R.id.textViewSelectedLongitude));
        Button buttonDone;
        buttonDone = (Button)findViewById(R.id.buttonSelectLocation);
        if (buttonDone != null) {
            buttonDone.setOnClickListener(this);
        }
        mRadiusText = (TextView)(findViewById(R.id.textViewDistanceValue));
        if (mRadiusText != null) {
            mRadiusText.setText(Double.toString(mRadius));
        }
        mRadiusSeekBar = (SeekBar)(findViewById(R.id.seekBarDistance));
        if (mRadiusSeekBar != null) {
            mRadiusSeekBar.setMax(MAX_RADIUS);
            mRadiusSeekBar.setProgress(mRadius.intValue());
            mRadiusSeekBar.setOnSeekBarChangeListener(this);
        }
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMapClickListener(this);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        enableMyLocation();
    }
    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
                // Permission to access the location is missing.
                PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        }
    }
    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng;
        if (mOriginalLatLng == null) {
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mOriginalLatLng = latLng;
        } else {
            latLng = mOriginalLatLng;
        }
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.animateCamera(cameraUpdate);
        if (!mPermissionDenied) {
            mLocationManager.removeUpdates(this);
        }
        mSelectedMarker = mMap.addMarker(new MarkerOptions()
                             .position(latLng)
                             .title("Reminder Place")
                             .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                             .draggable(true));
        mCircle = mMap.addCircle(new CircleOptions()
                    .center(latLng)
                    .radius(mRadius)
                    .strokeWidth(10)
                    .strokeColor(Color.BLACK)
                    .fillColor(Color.RED));
        mLatitudeText.setText(Double.toString(latLng.latitude));
        mLongitudeText.setText(Double.toString(latLng.longitude));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (mSelectedMarker != null) {
            mSelectedMarker.setPosition(latLng);
        }
        if (mCircle != null) {
            mCircle.setCenter(mSelectedMarker.getPosition());
            Log.d("MemoZone", "Circle moved");
        }
        mLatitudeText.setText(Double.toString(latLng.latitude));
        mLongitudeText.setText(Double.toString(latLng.longitude));
        mOriginalLatLng = latLng;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onInfoWindowClose(Marker marker) {

    }

    @Override
    public void onInfoWindowLongClick(Marker marker) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar == mRadiusSeekBar) {
            double doubleValue = (double)progress;
            mRadiusText.setText(String.valueOf(doubleValue));
            mRadius = doubleValue;
            mCircle.setRadius(mRadius);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSelectLocation:
                Intent intent = new Intent();
                intent.putExtra("latitude", Double.parseDouble(mLatitudeText.getText().toString()));
                intent.putExtra("longitude", Double.parseDouble(mLongitudeText.getText().toString()));
                intent.putExtra("radius", mRadius);
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
        }
    }
}
