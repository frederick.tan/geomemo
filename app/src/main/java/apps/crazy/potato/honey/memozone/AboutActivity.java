package apps.crazy.potato.honey.memozone;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

/**
 * Definition: About Activity
 * Date Created: 30/05/2016.
 * Author: Frederick Tan (frederick.tan@mensa.org.sg)
 */
public class AboutActivity
        extends Activity
        implements View.OnClickListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        ImageView okImageView = (ImageView)findViewById(R.id.aboutOk);
        if (okImageView != null) {
            okImageView.setOnClickListener(this);
        }
    }
    @Override
    public void onClick(View v) {
        finish();
    }
}
