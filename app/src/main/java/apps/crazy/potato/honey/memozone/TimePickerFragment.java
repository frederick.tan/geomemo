package apps.crazy.potato.honey.memozone;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import java.util.Calendar;

/**
 * Created by frede_000 on 30/05/2016.
 */
public class TimePickerFragment
        extends Fragment
        implements View.OnClickListener
{
    NumberPicker hourPicker;
    NumberPicker minutePicker;
    Calendar rightNow = Calendar.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.time_picker, container, false);
        hourPicker = (NumberPicker)view.findViewById(R.id.hourPicker);
        minutePicker = (NumberPicker)view.findViewById(R.id.minutePicker);
        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(23);
        hourPicker.setValue(rightNow.get(Calendar.HOUR_OF_DAY));
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(59);
        minutePicker.setValue(rightNow.get(Calendar.MINUTE));
        return view;
    }
    @Override
    public void onClick(View v) {

    }
}
